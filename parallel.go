package parallel

import (
	"runtime"
	"sync"
)

func Start(numThreads int, callback func(thread, total int)) {
	if numThreads <= 0 {
		numThreads = runtime.NumCPU()
	}
	var wg sync.WaitGroup
	wg.Add(numThreads)
	for i := 0; i < numThreads; i++ {
		go func(i, numThreads int) {
			callback(i, numThreads)
			wg.Done()
		}(i, numThreads)
	}
	wg.Wait()
}

func For(from int, to int, increment int, callback func(i, thread, total int)) {
	Start(-1, func(thread, total int) {
		for i := from; i < to; i += increment {
			if i%total == thread {
				callback(i, thread, total)
			}
		}
	})
}

func ForEach(elements []interface{}, callback func(i int, element interface{}, thread int, total int)) {

	For(0, len(elements), 1, func(i, thread, total int) {
		callback(i, elements[i], thread, total)
	})
}
